# Docker template

## Introduction

This is a repository containing the [Docker](https://www.docker.com/) files adopted by [UbuViz](http:/ubuviz.com) in order to have a seamlessly approach for installing and running applications that are built. This suppose that you have an operating system with `docker-ce` and `docker-compose` installed [following this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04).

First, add the GPG key for the official Docker repository to the system by typing this in the terminal on an Ubuntu OS:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Add the Docker repository to APT sources:

    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

Next, update the package database with the Docker packages from the newly added repo:

    sudo apt-get update

Finally, install Docker:

    sudo apt-get install -y docker-ce docker-compose


## Installation

First clone this repository :
      
    git clone https://iMitwe@bitbucket.org/iMitwe/dockertest.git <yourproject>

This template suppose that you have a **private repository** somewhere on [BitBucket](https://bitbucket.org) and is added in this template as submodule.

 Example, we added here the [`<yourproject>`](https://bitbucket.org/iMitwe/<yourproject>/src/master/) project in the folder `src` as  **<yourproject>**:
    
    cd dockertest/
    git submodule add  https://iMitwe@bitbucket.org/iMitwe/<yourproject>.git src/<yourproject>

First, we need to initialize the submodule(s). We can do that with the following command:

    git submodule init

Then we need to run the update in order to pull down the files.

    git submodule update

Looking in the `src/<yourproject>` directory now shows a nice listing of the needed files.

## Running docker

We are going to be running the docker image using the configurations present in the `Dockerfile` and `docker-compose.yml` files. You just need to adjust the settings in those files in order to match the settings you are using on your project. Especially the *ARG PROJECT=xxxx* parameter in the Dockerfile and fixes in the yaml file. 

Then you  run:
    
    docker-compose up --build -d

This downloads (or builds) and starts the three containers listed above in the *Yaml* file. You can view output from the containers by running:

    docker-compose logs

If all services launched successfully, you should now be able to access your application at http://localhost:8000/ in a web browser.

## Further reading

You can find extensive documentation on best practices that can be used to spin up a docker environment. Eg:

- [The Docker Ecosystem: An Introduction to Common Components](https://www.digitalocean.com/community/tutorials/the-docker-ecosystem-an-introduction-to-common-components) 

- [A Production-ready Dockerfile for Your Python/Django App](https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/ )
