# My Project
# Version: 1.0

FROM python:3.5

# Project Files and Settings
# ARG is the project you are working on. Please update accordingly.
ARG PROJECT=cabu

ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /src/${PROJECT}/requirements.pip /config/
RUN pip install --no-cache-dir -r /config/requirements.pip
RUN mkdir -p /src/${PROJECT};
WORKDIR /src/${PROJECT}